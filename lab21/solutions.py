# Name this file 'solutions.py'.
"""Volume II: Interior Point II (Quadratic Optimization).
Lehner White
Math 323
Mar 31, 2016
"""

import numpy as np
from scipy import linalg as la
from scipy.sparse import spdiags

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import axes3d

import cvxopt
from cvxopt import matrix

# Auxiliary function for problem 2
def startingPoint(G, c, A, b, guess):
    """
    Obtain an appropriate initial point for solving the QP
    .5 x^T Gx + x^T c s.t. Ax >= b.
    Inputs:
        G -- symmetric positive semidefinite matrix shape (n,n)
        c -- array of length n
        A -- constraint matrix shape (m,n)
        b -- array of length m
        guess -- a tuple of arrays (x, y, mu) of lengths n, m, and m, resp.
    Returns:
        a tuple of arrays (x0, y0, l0) of lengths n, m, and m, resp.
    """
    m,n = A.shape
    x0, y0, l0 = guess

    N = np.zeros((n+m+m, n+m+m))
    N[:n,:n] = G
    N[:n, n+m:] = -A.T
    N[n:n+m, :n] = A
    N[n:n+m, n:n+m] = -np.eye(m)
    N[n+m:, n:n+m] = np.diag(l0)
    N[n+m:, n+m:] = np.diag(y0)
    rhs = np.empty(n+m+m)
    rhs[:n] = -(G.dot(x0) - A.T.dot(l0)+c)
    rhs[n:n+m] = -(A.dot(x0) - y0 - b)
    rhs[n+m:] = -(y0*l0)

    sol = la.solve(N, rhs)
    dx = sol[:n]
    dy = sol[n:n+m]
    dl = sol[n+m:]

    y0 = np.maximum(1, np.abs(y0 + dy))
    l0 = np.maximum(1, np.abs(l0+dl))

    return x0, y0, l0

# Problems 1-2
def qInteriorPoint(Q, c, A, b, guess, niter=20, tol=1e-16, verbose=False):
    """Solve the Quadratic program min .5 x^T Q x +  c^T x, Ax >= b
    using an Interior Point method.

    Parameters:
        Q ((n,n) ndarray): Positive semidefinite objective matrix.
        c ((n, ) ndarray): linear objective vector.
        A ((m,n) ndarray): Inequality constraint matrix.
        b ((m, ) ndarray): Inequality constraint vector.
        guess (3-tuple of arrays of lengths n, m, and m): Initial guesses for
            the solution x and lagrange multipliers y and eta, respectively.
        niter (int > 0): The maximum number of iterations to execute.
        tol (float > 0): The convergence tolerance.

    Returns:
        x ((n, ) ndarray): The optimal point.
        val (float): The minimum value of the objective function.
    """
    def F(x, y, mu):
        f1 = np.dot(Q, x) - np.dot(A.T, mu) + c
        f2 = np.dot(A, x) - y - b
        f3 = np.dot(np.diag(y), np.diag(mu)).dot(np.ones(len(mu)))
        return np.hstack((f1,f2,f3))
    
    def direction(Q,y, sigma = .1):
        m, n = np.shape(A)
        n_zero = np.zeros((n,n))
        m_zero = np.zeros((m,m))
        mn_zeros = np.zeros((m,n))
        nm_zeros = np.zeros((n,m))
        
        Y = np.diag(y)
        M = np.diag(mu)
        nu = np.dot(y.T, mu)/ m
        
        DF1 = np.vstack((Q, A, mn_zeros))
        DF2 = np.vstack((nm_zeros, -1*np.eye(m), M))
        DF3 = np.vstack((-1*A.T, m_zero, Y))
        DF = np.hstack((DF1, DF2, DF3))
        
        B = -1*F(x,y,mu) + np.hstack((np.zeros((n+m)), sigma*nu*np.ones(m)))
        sol =  la.solve(DF, B)
        dx = sol[:n]
        dy = sol[n:n+m]
        dmu = sol[n+m:] 
        return dx, dy, dmu
    
    def size(dy, dmu):
        m, n = A.shape
        nu = np.dot(y.T, mu)/ m

        mu_mask = dmu < 0
        y_mask = dy < 0

        b_max = np.min(np.append(1., -1*mu[mu_mask]/dmu[mu_mask]))
        d_max = np.min(np.append(1., -1*y[y_mask]/dy[y_mask]))
        
        tau = 0.95

        beta = min(1., tau*b_max)
        delta = min(1., tau*d_max)

        alph = min(beta, delta)

        return alph

    m, n = A.shape
    x, y, mu = startingPoint(Q, c, A, b, guess)
    nu = np.dot(y.T, mu)/ m
    i = 0

    while nu > tol and i < niter:
        dx, dy, dmu = direction(Q, y)
        alph = size(dy, dmu)
        x += alph*dx
        y += alph*dy
        mu += alph*dmu
        
        nu = np.dot(y.T, mu)/ m
        i+=1
    return x, .5*np.dot(x.T, Q).dot(x) + np.dot(c.T, x) 

def testInterior():
    Q = np.array([[1.,-1.], [-1.,2.]])
    c = np.array([[-2.],[-6]]).flatten()
    A = np.array([[-1.,-1.],[1.,-2.],[-2.,-1.],[1.,0.],[0.,1.]])
    b = np.array([[-2.],[-2.],[-3.],[0],[0]]).flatten()
    guess = np.array([np.ones(Q.shape[1])]).flatten()*.5,np.array([np.ones(A.shape[0])]).flatten(), np.array([np.ones(A.shape[0])]).flatten()
    point, value = qInteriorPoint(Q,c,A,b,guess)
    print point, value
    
#testInterior() 


# Auxiliary function for problem 3
def laplacian(n):
    """Construct the discrete Dirichlet energy matrix H for an n x n grid."""
    data = -1*np.ones((5, n**2))
    data[2,:] = 4
    data[1, n-1::n] = 0
    data[3, ::n] = 0
    diags = np.array([-n, -1, 0, 1, n])
    return spdiags(data, diags, n**2, n**2).toarray()

# Problem 3
def circus(n=15):
    """Solve the circus tent problem for grid size length 'n'.
    Plot and show the solution.
    """
    c = np.ones(n**2)*-(n - 1.)**-2.
    A = np.eye(n**2)
    H = laplacian(n)

    #Create the tent pole configuration
    L = np.zeros((n,n))
    L[n//2-1:n//2+1,n//2-1:n//2+1] = .5
    m = [n//6-1, n//6, int(5*(n/6.))-1, int(5*(n/6.))]
    mask1, mask2 = np.meshgrid(m, m)
    L[mask1, mask2] = .3
    L = L.ravel()
    
    # Set initial guesses.
    x = np.ones((n,n)).ravel()
    y = np.ones(n**2)
    mu = np.ones(n**2)
    
    # Calculate the solution.
    z = qInteriorPoint(H, c, A, L, (x,y,mu))[0].reshape((n,n))
    
    # Plot the solution.
    domain = np.arange(n)
    X, Y = np.meshgrid(domain, domain)
    fig = plt.figure()
    ax1 = fig.add_subplot(111, projection='3d')
    ax1.plot_surface(X, Y, z, rstride=1, cstride=1, color='r')
    plt.show()
#circus()
    


# Problem 4
def portfolio(filename="portfolio.txt"):
    """Use the data in the specified file to estimate a covariance matrix and
    expected rates of return. Find the optimal portfolio that guarantees an
    expected return of R = 1.13, with and then without short selling.

    Returns:
        An array of the percentages per asset, allowing short selling.
        An array of the percentages per asset without allowing short selling.
    """
    data = np.loadtxt(filename)

    R = 1.13
    mu = np.mean(data[:,1:], axis = 0)
    Q = matrix(np.cov(data[:,1:].T))
    n = np.cov(data[:,1:].T).shape[0]
    A = matrix(np.vstack((np.ones(n), mu)))
    G = matrix(-1*np.eye(n))
    h = matrix(np.zeros(n))
    b = matrix(np.array([1., R]))
    
    sol_short = cvxopt.solvers.qp(Q, h, A=A, b=b)
    sol = cvxopt.solvers.qp(Q, h, G, h, A=A, b=b)
    
    ans1 = np.ravel(np.array(sol_short['x']))
    ans2 = np.ravel(np.array(sol['x']))
    return ans1, ans2

def test4():
    res1, res2 = portfolio('ACME_VIM/volume2/lab21/portfolio.txt')
    print np.sum(res1)
    print res1
    print np.sum(res2)
    print res2
#test4()

