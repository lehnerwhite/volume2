# name this file 'solutions.py'.
"""Volume II Lab 15: Line Search Algorithms
<name>
<class>
<date>
"""
import numpy as np
from scipy import linalg as la
from scipy.optimize import leastsq
from matplotlib import pyplot as plt

# Problem 1
def newton1d(f, df, ddf, x, niter=10):
    """
    Perform Newton's method to minimize a function from R to R.

    Parameters:
        f (function): The twice-differentiable objective function.
        df (function): The first derivative of 'f'.
        ddf (function): The second derivative of 'f'.
        x (float): The initial guess.
        niter (int): The number of iterations. Defaults to 10.
    
    Returns:
        (float) The approximated minimizer.
    """
    for i in xrange(niter):
        x2 = x - df(x)/ddf(x)
        x = x2
    return x2

def test_newton():
    """Use the newton1d() function to minimixe f(x) = x^2 + sin(5x) with an
    initial guess of x_0 = 0. Also try other guesses farther away from the
    true minimizer, and note when the method fails to obtain the correct
    answer.

    Returns:
        (float) The true minimizer with an initial guess x_0 = 0.
        (float) The result of newton1d() with a bad initial guess.
    """
    f = lambda x : x**2 + np.sin(5*x)
    df = lambda x : 2*x + 5*np.cos(5*x)
    ddf = lambda x : 2 - 25*np.sin(5*x)
    
    print newton1d(f, df, ddf, 0.) 
    print newton1d(f, df, ddf, .1) 
    print newton1d(f, df, ddf, .101) 
    print newton1d(f, df, ddf, .125) 
    print newton1d(f, df, ddf, .15) 
    #Apparently any value past .1 will return a bad value
    return newton1d(f, df, ddf, 0.), newton1d(f, df, ddf, .101)

#test_newton()
    

# Problem 2
def backtracking(f, slope, x, p, a=1, rho=.9, c=10e-4):
    """Perform a backtracking line search to satisfy the Armijo Conditions.

    Parameters:
        f (function): the twice-differentiable objective function.
        slope (float): The value of grad(f)^T p.
        x (ndarray of shape (n,)): The current iterate.
        p (ndarray of shape (n,)): The current search direction.
        a (float): The intial step length. (set to 1 in Newton and
            quasi-Newton methods)
        rho (float): A number in (0,1).
        c (float): A number in (0,1).
    
    Returns:
        (float) The computed step size satisfying the Armijo condition.
    """
    while f(x + a*p) > (f(x) + c * a * c * slope):
        a = rho*a
    return a

# Problem 3    
def gradientDescent(f, df, x, niter=10):
    """Minimize a function using gradient descent.

    Parameters:
        f (function): The twice-differentiable objective function.
        df (function): The gradient of the function.
        x (ndarray of shape (n,)): The initial point.
        niter (int): The number of iterations to run.
    
    Returns:
        (list of ndarrays) The sequence of points generated.
    """
    pts = [x]
    for i in xrange(niter):
        p = df(x)
        slope  = np.dot(p, p)
        a = backtracking(f, slope, x, p)
        x = x + a*p
        pts.append(x)
    return pts

def newtonsMethod(f, df, ddf, x, niter=10):
    """Minimize a function using Newton's method.

    Parameters:
        f (function): The twice-differentiable objective function.
        df (function): The gradient of the function.
        ddf (function): The Hessian of the function.
        x (ndarray of shape (n,)): The initial point.
        niter (int): The number of iterations.
    
    Returns:
        (list of ndarrays) The sequence of points generated.
    """
    pts = [x]
    for i in xrange(niter):
        p = la.solve(ddf(x), -1*df(x))
        slope = np.dot(df(x), p)
        a = backtracking(f, slope, x, p)
        x = x + a*p
        pts.append(x)
    return pts


# Problem 4
def gaussNewton(f, df, jac, r, x, niter=10):
    """Solve a nonlinear least squares problem with Gauss-Newton method.

    Parameters:
        f (function): The objective function.
        df (function): The gradient of f.
        jac (function): The jacobian of the residual vector.
        r (function): The residual vector.
        x (ndarray of shape (n,)): The initial point.
        niter (int): The number of iterations.
    
    Returns:
        (ndarray of shape (n,)) The minimizer.
    """
    for i in xrange(niter):
        p = -1*np.dot(np.linalg.inv(np.dot(jac(x).T, jac(x))), np.dot(jac(x).T,r(x)))
        slope = np.dot(df(x), p)
        a = backtracking(f, slope, x, p)
        x = x + a*p
    return x 

def test4():
    t = np.arange(10)
    y = 3*np.sin(0.5*t)+ 0.5*np.random.randn(10)
     
    def model(x, t):
        return x[0]*np.sin(x[1]*t)
    def residual(x):
        return model(x, t) - y
    def jac(x):
        ans = np.empty((10,2))
        ans[:,0] = np.sin(x[1]*t)
        ans[:,1] = x[0]*t*np.cos(x[1]*t)
        return ans
    def objective(x):
        return .5*(residual(x)**2).sum()
    def grad(x):
        return jac(x).T.dot(residual(x))
    x0 = np.array([2.5,.6])
    x = gaussNewton(objective, grad, jac, residual, x0, niter=10)
     
    dom = np.linspace(0,10,100) 
    plt.plot(t, y, '*')
    plt.plot(dom, 3*np.sin(.5*dom), '--') 
    plt.plot(dom, x[0]*np.sin(x[1]*dom)) 
    plt.show()
#test4()

# Problem 5
def census():
    """Generate two plots: one that considers the first 8 decades of the US
    Census data (with the exponential model), and one that considers all 16
    decades of data (with the logistic model).
    """

    # Start with the first 8 decades of data.
    years1 = np.arange(8)
    pop1 = np.array([3.929,  5.308,  7.240,  9.638,
                    12.866, 17.069, 23.192, 31.443])

    # Now consider the first 16 decades.
    years2 = np.arange(16)
    pop2 = np.array([3.929,   5.308,   7.240,   9.638,
                    12.866,  17.069,  23.192,  31.443,
                    38.558,  50.156,  62.948,  75.996,
                    91.972, 105.711, 122.775, 131.669])

    def model(x,t):
        return x[0] * np.exp(x[1]*(years1 + x[2]))

    def residual(x):
        return model(x, years1) - pop1

    x0 = np.array([150., .4, 2.5])
    x = leastsq(residual, x0)[0]

    dom = np.linspace(0,8,100) 
    plt.plot(years1, pop1, '*')
    plt.plot(dom, x[0]*np.exp(x[1]*(dom+x[2])))
    plt.show()

    def model(x,t):
        return x[0]/(1+np.exp(-1*x[1]*(years2 + x[2])))

    def residual(x):
        return model(x, years2) - pop2

    x0 = np.array([150., .4, -15])
    x = leastsq(residual, x0)[0]

    dom = np.linspace(0,16,100) 
    plt.plot(years2, pop2, '*')
    plt.plot(dom, x[0]/(1+np.exp(-1*x[1]*(dom + x[2]))))
    plt.show()

#census()
