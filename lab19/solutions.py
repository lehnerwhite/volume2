# Name this file 'solutions.py'
"""Volume II Lab 19: Trust Region Methods
Lehner White
Math 323
Mar 17
"""
import numpy as np
from scipy import linalg as la
from scipy import optimize as op
from scipy.optimize import fsolve


# Problem 1
def trustRegion(f,grad,hess,subprob,x0,r0,rmax=2.,eta=1./16,gtol=1e-5):
    """Implement the trust regions method.
    
    Parameters:
        f (function): The objective function to minimize.
        g (function): The gradient (or approximate gradient) of the objective
            function 'f'.
        hess (function): The hessian (or approximate hessian) of the objective
            function 'f'.
        subprob (function): Returns the step p_k.
        x0 (ndarray of shape (n,)): The initial point.
        r0 (float): The initial trust-region radius.
        rmax (float): The max value for trust-region radii.
        eta (float in [0,0.25)): Acceptance threshold.
        gtol (float): Convergence threshold.
        
    
    Returns:
        x (ndarray): the minimizer of f.
    
    Notes:
        The functions 'f', 'g', and 'hess' should all take a single parameter.
        The function 'subprob' takes as parameters a gradient vector, hessian
            matrix, and radius.
    """
    while np.linalg.norm(grad(x0)) > gtol:
        p = subprob(grad(x0), hess(x0), r0)
        rho = (f(x0) - f(x0 + p)) / (-1*np.dot(p, grad(x0)) - .5*np.dot(p.T, np.dot(hess(x0), p)))
        if rho < 0.25:
            r0 *= .25
        else: 
            if rho > 0.75 and np.linalg.norm(p):
                r = np.min([2*r0, rmax])
            else:
                pass
        if rho > eta:
            x0 = x0 + p
        else:
            pass
    return x0

# Problem 2   
def dogleg(gk,Hk,rk):
    """Calculate the dogleg minimizer of the quadratic model function.
    
    Parameters:
        gk (ndarray of shape (n,)): The current gradient of the objective
            function.
        Hk (ndarray of shape (n,n)): The current (or approximate) hessian.
        rk (float): The current trust region radius
    
    Returns:
        pk (ndarray of shape (n,)): The dogleg minimizer of the model function.
    """
    pB = la.solve(-1*Hk, gk)
    pU = -1 * np.dot(((np.dot(gk.T, gk))/(np.dot(gk.T, np.dot(Hk, gk)))), gk)
    
    def gamma(tau):
        if tau >= 0 and tau <= 1:
           return tau*pU 
        elif tau > 1 and tau <= 2:
            return tau*pU + (tau - 1)(pB - pU)
    
    if np.linalg.norm(pB) < rk:
        return pB
    elif np.linalg.norm(pB) > rk:
        return rk/np.linalg.norm(pU) * pU
    elif np.linalg.norm(pB) == rk:
        a = np.dot(pB.T, pB) - 2.*np.dot(pB, pU) + np.dot(pU.T, pU)
        b = 2.*np.dot(pB.T, pU) - 2.*np.dot(pU.T, pU)
        c = np.dot(pU, pU) - rk**2
        roots = np.roots([a,(b-2*a),(a-b+c)])
        tau_star = max(roots)
        return pU + (tau_star - 1)(pB-pU)

# Problem 3
def problem3():
    """Test your trustRegion() method on the Rosenbrock function.
    Define x0 = np.array([10.,10.]) and r = .25
    Return the minimizer.
    """
    x = np.array([10.,10])
    rmax=2.
    r=.25
    eta=1./16
    tol=1e-5
    opts = {'initial_trust_radius':r, 'max_trust_radius':rmax, 'eta':eta, 'gtol':tol}
    sol1 = op.minimize(op.rosen, x, method='dogleg', jac=op.rosen_der, hess=op.rosen_hess, options=opts)
    sol2 = trustRegion(op.rosen, op.rosen_der, op.rosen_hess, dogleg, x, r, rmax, eta, gtol=tol)
    #print np.allclose(sol1.x, sol2)
    return sol1

# Problem 4
def problem4():
    """Solve the described non-linear system of equations.
    Return the minimizer.
    """
    # define the system of equations
    def r(x):
        return np.array([np.sin(x[0])*np.cos(x[1]) - 4*np.cos(x[0])*np.sin(x[1]), 
                            np.sin(x[1])*np.cos(x[0]) - 4*np.cos(x[1])*np.sin(x[0])])
    
    # define the merit function
    def f(x):
        return .5*(r(x)**2).sum()

    # define the jacobian function
    def J(x):
        return np.array([[np.cos(x[0])*np.cos(x[1]) + 4*np.sin(x[0])*np.sin(x[1]),
                            -np.sin(x[0])*np.sin(x[1]) - 4*np.cos(x[0])*np.cos(x[1])]
                        ,[-np.sin(x[1])*np.sin(x[0]) - 4*np.cos(x[1])*np.cos(x[0]),
                            np.cos(x[1])*np.cos(x[0]) + 4*np.sin(x[1])*np.sin(x[0])]])
    
    # define the gradient function
    def g(x):
        return J(x).dot(r(x))

    # define the Hessian function
    def H(x):
        return J(x).T.dot(J(x))

    # set trust-region parameters
    rmax=2.
    rr=.25
    eta=1./16
    tol=1e-5
    
    # set initial point
    x = np.array([3.5, -2.5])
    
    # find a minimizer of f
    xstar = trustRegion(f,g,H,dogleg,x,rr,rmax,eta=eta,gtol=tol)
    #print xstar
    
    # verify that it is a root of r
    #print r(xstar)
    return xstar
