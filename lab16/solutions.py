# name this file 'solutions.py'.
"""Volume II Lab 16: Simplex
Lehner White
Math 323
Jan 28, 2016

Problems 1-6 give instructions on how to build the SimplexSolver class.
The grader will test your class by solving various linear optimization
problems and will only call the constructor and the solve() methods directly.
Write good docstrings for each of your class methods and comment your code.

prob7() will also be tested directly.
"""
import numpy as np
from copy import deepcopy as copy

# Problems 1-6
class SimplexSolver(object):
    """Class for solving the standard linear optimization problem

                        maximize        c^Tx
                        subject to      Ax <= b
                                         x >= 0
    via the Simplex algorithm.
    """
    
    def __init__(self, c, A, b):
        """
        The initialization method will take in the objective function and constraints, 
        first check to assure that the origin is feasible, build a list of basic and 
        non-basic variables, and then initialize our tableau that we will be able to 
        use in the simplex algorithm. 

        Parameters:
            c (1xn ndarray): The coefficients of the linear objective function.
            A (mxn ndarray): The constraint coefficients matrix.
            b (1xm ndarray): The constraint vector.

        Raises:
            ValueError: if the given system is infeasible at the origin.
        """
        self.c = c
        self.A = A
        self.b = np.reshape(b, (len(b), 1))
        self.m = A.shape[0]
        self.n = A.shape[1]
        self.mn = sum(A.shape)
        
        # Verify that the origin is feasible
        if np.any(self.b < 0):
            raise ValueError("The origin is not feasible")
        
        # Creating our list of basic and non-basic variables
        L1 = [i for i in xrange(self.n, self.mn)]
        L2 = [i for i in xrange(self.n)]
        self.L = L1+L2
        
        # Build our tableau with the parameters that we are passed
        c2 = np.zeros(self.m)
        self.c = np.hstack((-1*self.c, c2))

        A2 = np.eye(self.m)
        self.A = np.hstack((self.A, A2))

        T1 = np.hstack((0, self.c.T, 1))
        T2 = np.hstack((self.b, self.A, np.zeros((self.m,1))))
        self.T = np.vstack((T1, T2))

    def determine_pivot(self):
        """
        This method will determine the pivot column by checking the sign of the values 
        in the first row of the tableau and then compare ratios to determine the pivot 
        row, using Bland's Rule to resolve any complications. 

        Returns:
        -------
            (tuple): the pivot row and the pivot column

        """
        # We find the first negative value in the first row to determine our pivot column
        candidates = copy(self.T[0,1:])
        col = copy(candidates[0])
        i=0
        while col >= 0:
            i+=1
            col = candidates[i]
        candidates = copy(self.T[1:, i+1])
        
        # We verify that this column does not show the problem is unbounded
        for ii in xrange(self.n):
            if np.all(self.T[1:,ii+1] < 0):
                raise ValueError("The problem is unbounded.")
        
        # Here we convert negative and zero numbers to a very small positive number to cause very large ratios that will never be the minimum
        candidates[candidates <=0] = 10e-20
        
        # Find the smallest ratio and store this as our pivot row
        candidates = copy(self.T[1:,0]) / candidates
        j = self.L[np.argmin(candidates[candidates >= 0])]

        # The previous code will simply find the first smallest ratio in a tie, here we check for multiples and assure that our pivot row is the row with the smallest index of the leaving variable, AKA assures Bland's Rule is followed
        multiples = []
        for idx, cand in enumerate(candidates):
            if cand == candidates[np.argmin(candidates)]:
                multiples.append(idx)
        if len(multiples) > 1:
            indices = []
            for mult in multiples:
                indices.append(self.L[mult+1])
            j = self.L[multiples[np.argmin(indices)]]

        return j, i
    
    def pivot(self):
        """
        This method will use the determine_pivot method to get the pivot row and column, 
        find the indices for these variables in the list of variables and exchange the 
        values. It will then pivot the tableau to contain the accurate values. 
        """
        j, i = self.determine_pivot()
        
        # We want to find the indices of the proper variables in our variable list
        idx_j = self.L.index(j)
        idx_i = self.L.index(i)
        
        # We swap the positions of the entering and leaving variables
        self.L[idx_j], self.L[idx_i] = self.L[idx_i], self.L[idx_j] 

        # We perform the actual pivot on our tableau
        row = j - (self.n - 1)
        column = i + 1
        self.T[row,:] /= self.T[row, column]
        for ii in xrange(self.m+1):
            if (ii != row):
                self.T[ii, :] -= self.T[row,:] * self.T[ii, column]
        
    def solve(self):
        """
        Solve the linear optimization problem.

        Returns:
            (float): The maximum value of the objective function.
            (dict): The basic variables and their values.
            (dict): The nonbasic variables and their values.
        """
        # Keep pivoting as long as we have negative values in C
        while not np.all(self.T[0,:] >= 0):
            self.pivot()
        
        #Store optimum value    
        max_val = self.T[0,0]

        # Store dictionary with basic variables and their values
        basic = {}
        for ii in xrange(self.m):
            basic[self.L[ii]] = self.T[1:,0][ii]
        
        # Store dictionary with non-basic variables and their values
        nonbasic = {}
        for ii in xrange(self.m, self.mn):
            nonbasic[self.L[ii]] = 0

        return (max_val, basic, nonbasic)

def testing():
    # Initialize objective function and constraints.
    c = np.array([3., 2])
    b = np.array([2., 5, 7])
    A = np.array([[1., -1], [3, 1], [4, 3]])

    # Instantiate the simplex solver, then solve the problem.
    solver = SimplexSolver(c, A, b)
    sol = solver.solve()
    print(sol)

# Problem 7
def prob7(filename='productMix.npz'):
    """Solve the product mix problem for the data in 'productMix.npz'.

    Parameters:
        filename (str): the path to the data file.

    Returns:
        The minimizer of the problem (as an array).
    """
    #data = np.load('ACME_VIM/volume2/lab16/productMix.npz')
    data = np.load(filename)
    c = data['p']
    A = np.vstack((data['A'], np.eye(len(data['d']))))
    b = np.hstack((data['m'], data['d']))
    solver = SimplexSolver(c, A, b)
    sol = solver.solve()
    solution = {}
    print("The optimal revenue is ${}".format('%.2f' % sol[0]))
    for good in sol[1]:
        solution[good] = sol[1][good]
    for good in sol[2]:
        solution[good] = sol[2][good]
    #print np.dot(c,solution.values()[:len(c)])
    return solution.values()[:len(c)]
     
# END OF FILE =================================================================


