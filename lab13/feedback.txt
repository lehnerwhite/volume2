01/14/16 17:02 

Problem 1 (10 points):
Score += 10

Problem 2 (10 points):
Score += 10

Problem 3 (10 points):
Score += 10

Problem 4 (10 points):
Score += 10

Total score: 40/40 = 100.0%

Excellent!


Comment:
    In the future, when you have a data file to load you
    may assume that it is stored in the same file as the
    solutions script.

    data = np.loadtxt('ACME_VIM/volume2/lab13/heating.txt') <- not this
    data = np.loadtxt('heating.txt')                        <- this

    I will make sure other spec files are more specific
    to fix this. Good work!
-------------------------------------------------------


