# Name this file 'solutions.py'.
"""Volume II: Compressed Sensing.
Lehner White
Math 323
Feb 18, 2016
"""
from cvxopt import matrix, solvers
import numpy as np
from matplotlib import pyplot as plt
from camera import Camera
from visualize2 import visualizeEarth

# Problem 1
def l1Min(A, b):
    """Calculate the solution to the optimization problem

        minimize    ||x||_1
        subject to  Ax = b

    Return only the solution x (not any slack variable), as a flat NumPy array.

    Parameters:
        A ((m,n) ndarray)
        b ((m, ) ndarray)

    Returns:
        x ((n, ) ndarray): The solution to the minimization problem.
    """
    m, n = A.shape
    
    c1 = np.ones(n)
    c2 = np.zeros(n)
    c = np.hstack((c1, c2))

    I = np.eye(n)
    G1 = np.hstack((-1*I, I))
    G2 = np.hstack((-1*I, -1*I))
    G = np.vstack((G1, G2))

    h = np.zeros(2*n)

    E = np.hstack((np.zeros(A.shape), A))

    E = matrix(E)
    f = matrix(b)

    c = matrix(c)
    G = matrix(G)
    h = matrix(h)
    sol = solvers.lp(c, G, h, E, f)
    return sol['x'][n:]

def test_prob1():
    A = np.array([[1.,2.,0.],[2.,1.,3.]])
    b = np.array([3.,10.])
    print l1Min(A,b)


# Problem 2
def prob2(filename='ACME.png'):
    """Reconstruct the image in the indicated file using 100, 200, 250,
    and 275 measurements. Seed NumPy's random number generator with
    np.random.seed(1337) before each measurement to obtain consistent
    results.

    Resize and plot each reconstruction in a single figure with several
    subplots (use plt.imshow() instead of plt.plot()). Return a list
    containing the Euclidean distance between each reconstruction and the
    original image.
    """
    distances = []
    for idx, sam in enumerate([100,200,250,275]):
        acme = 1 - plt.imread('ACME.png')[:,:,0]
        m = sam                     # The number of measurements.
        np.random.seed(1337)        # Seed the random generator for consistency.
        
        A = np.random.randint(low=0, high=2, size=(m, 32**2))
        b = A.dot(acme.flatten())
        reconstruction = np.reshape(l1Min(A,b), (32,32))
        distances.append(np.linalg.norm(acme-reconstruction))
        plt.subplot(2,2,(idx+1))
        plt.title('Measurement: {}'.format(sam))
        plt.axis('off')
        plt.imshow(reconstruction)
    plt.show()
    return distances

# Problem 3
def prob3(filename="StudentEarthData.npz"):
    """Reconstruct single-pixel camera color data in StudentEarthData.npz
    using 450, 650, and 850 measurements. Seed NumPy's random number generator
    with np.random.seed(1337) before each measurement to obtain consistent
    results.

    Return a list containing the Euclidean distance between each
    reconstruction and the color array.
    """
    data = np.load('ACME_VIM/volume2/lab17/StudentEarthData.npz')
    #data = np.load(filename)
    faces = data['faces']
    vertices = data['vertices']
    colors = data['C']
    v = data['V']
    
    visualizeEarth(faces,vertices, colors)
    
    distances = []

    for idx, m in enumerate([450,650,850]):
        myCamera = Camera(faces, vertices, colors)
        myCamera.add_lots_pic(m)
        A, B = myCamera.returnData()
        
        b1 = B[:,0]
        b2 = B[:,1]
        b3 = B[:,2]

        p1 = l1Min(np.dot(A,v), b1)
        p2 = l1Min(np.dot(A,v), b2)
        p3 = l1Min(np.dot(A,v), b3)

        s1 = np.dot(v,p1)
        s2 = np.dot(v,p2)
        s3 = np.dot(v,p3)

        S = np.column_stack((s1,s2,s3))

        visualizeEarth(faces, vertices, S.clip(0,1))

        distances.append(np.linalg.norm(colors - S))
    return distances

print prob3()
    

