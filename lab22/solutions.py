# Name this file 'solutions.py'.
"""Volume II Lab 22: Dynamic Optimization (Value Function Iteration).
Lehner White
Math 323
Apr 7, 2016
"""
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D


def eatCake(beta, N, W_max=1, T=None, finite=True, plot=False):
    """Solve the finite- or infinite-horizon cake-eating problem using
    Value Function iteration.
    
    Inputs:
        beta (float): Discount factor.
        N (int): The number of discrete cake values.
        W_max (int): The original size of the cake.
        T (int): The final time period. Defaults to None.
        finite (bool): If True, solve the finite-horizon problem. If False,
            solve the infinite-horizon problem.
        plot (bool): If True, plot the value function surface and policy
            function.

    Returns:
        values ((N, T+2) ndarray if finite=True, (N,) ndarray if finite=False):
            The value function at each time period for each state (this is also
            called V in the lab).
        psi ((N, T+1) ndarray if finite=True, (N,) ndarray if finite=False):
            The policy at each time period for each state.
    """
    w = np.linspace(0,W_max, N)
    W = np.empty((N,N)) 
    for i in xrange(N):
        for j in xrange(N):
            W[i,j] = w[i] - w[j]        
    
    W_mask = W < 0
    W[W_mask] = 0
    W = np.sqrt(W)
    W[W_mask] = -10e10
      
    if finite==True:
        V = np.zeros((N,T+2))
        psi = np.zeros((N,T+1))
        for t in reversed(xrange(T)):
            W_next = W + (beta * V[:,t+1]).reshape((1,N))
            V[:,t] = np.max(W_next, axis = 1)
            idx = np.argmax(W_next, axis = 1)
            psi[:,t] = (W_next[:,idx[0]])
        
        if plot:
            W = np.linspace(0, W_max, N)
            x = np.arange(0, N)
            y = np.arange(0, T+2)
            X, Y = np.meshgrid(x, y)
            fig1 = plt.figure()
            ax1 = Axes3D(fig1)
            ax1.plot_surface(W[X], Y, np.transpose(V), cmap=cm.coolwarm)
            plt.show()
            fig2 = plt.figure()
            ax2 = Axes3D(fig2)
            y = np.arange(0,T+1)
            X, Y = np.meshgrid(x, y)
            ax2.plot_surface(W[X], Y, np.transpose(psi), cmap=cm.coolwarm)
            plt.show()
    
    if finite==False:
        delt = np.inf
        V = np.zeros(N)
        while delt > 10e-9:
            W_next = W + (beta * V).reshape((1,N))
            V2 = np.max(W_next, axis = 1)
            psi = np.argmax(W_next, axis = 1)/float(N)
            delt = np.dot((V2-V).T, (V2-V))
            V = V2
        
        if plot:
            plt.plot(w, psi)
            plt.show()

    return V, psi

#eatCake(.9, 100, T=10, plot=True)

def prob2():
    """Call eatCake() with the parameters specified in the lab."""
    eatCake(.9, 100, T=1000, plot=True)
#prob2() 

def prob3():
    """Modify eatCake() to deal with the infinite case.
    Call eatCake() with the parameters specified in part 6 of the problem.
    """
    eatCake(.9, 100, finite=False, plot=True)
#prob3()
