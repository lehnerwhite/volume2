# Name this file 'solutions.py'.
"""Volume II Lab 18: Conjugate Gradient
Lehner White
Math 323
Feb 25, 2016
"""
import numpy as np
from scipy import optimize
from copy import copy

# Problem 1
def conjugateGradient(b, x0, Q, tol=1e-4):
    """Use the Conjugate Gradient Method to find the solution to the linear
    system Qx = b.
    
    Parameters:
        b  ((n, ) ndarray)
        x0 ((n, ) ndarray): An initial guess for x.
        Q  ((n,n) ndarray): A positive-definite square matrix.
        tol (float)
    
    Returns:
        x ((n, ) ndarray): The solution to the linear systm Qx = b, according
            to the Conjugate Gradient Method.
    """
    r0 = np.dot(Q, x0) - b
    d = -1 * r0
    k = 0
    while np.linalg.norm(r0) >= tol:
        a = np.dot(r0.T, r0) / np.dot(np.dot(d.T, Q), d)
        x1 = x0 + a * d
        r1 = r0 + a * np.dot(Q, d)
        beta = np.dot(r1.T, r1) / np.dot(r0.T,r0)
        d = -1 * r1 + beta * d
        x0 = x1
        r0 = r1

    return x0    

# Problem 2
def prob2(filename='linregression.txt'):
    """Use conjugateGradient() to solve the linear regression problem with
    the data from linregression.txt.
    Return the solution x*.
    """
    data = np.loadtxt(filename)
    m, n = data.shape
    
    b = data[:,0].T
    A = np.hstack((np.ones((m, 1)), copy(data[:,1:])))
    AA = np.dot(A.T, A)
    Ab = np.dot(A.T, b)
    
    x0 = np.random.random(n)
    x = conjugateGradient(Ab, x0, AA)
    
    return x

# Problem 3
def prob3(filename='logregression.txt'):
    """Use scipy.optimize.fmin_cg() to find the maximum likelihood estimate
    for the data in logregression.txt.
    """
    data = np.loadtxt(filename)
    m,n = data.shape

    est = np.ones((n))
    x = np.ones((m,n))
    x[:,1:] = data[:,1:]
    y = data[:,0]
    obj = lambda b : np.sum(np.log(1+np.exp(x.dot(b))) - y*(x.dot(b))) 
    b = optimize.fmin_cg(obj, est)
    
    return b

