# name this file solutions.py
"""Volume 2 Lab 14: Optimization Packages II (CVXOPT)
Lehner White
Math 323
Jan 14, 2016
"""
from cvxopt import matrix
from cvxopt import solvers
import numpy as np

def prob1():
    """Solve the following convex optimization problem:

    minimize        2x + y + 3z
    subject to      x + 2y          >= 3
                    2x + y + 3z     >= 10
                    x               >= 0
                    y               >= 0
                    z               >= 0

    Returns (in order):
        The optimizer (sol['x'])
        The optimal value (sol['primal objective'])
    """
    c = matrix([2., 1., 3.])
    G = matrix([[-1.,-2.,-1.,0.,0.],[-2.,-1.,0.,-1.,0.],[0.,-3.,0.,0.,-1.]])
    h = matrix([-3.,-10.,0.,0.,0.])
    sol = solvers.lp(c,G,h)
    return sol['x'], sol['primal objective']

#prob1()

def prob2():
    """Solve the transportation problem by converting all equality constraints
    into inequality constraints.

    Returns (in order):
        The optimizer (sol['x'])
        The optimal value (sol['primal objective'])
    """
    c = matrix([4., 7., 6., 8., 8., 9])
    G = matrix([[-1.,0.,0.,0.,0.,0., 1., 0., 0., 1., 0., -1., 0., 0., -1., 0.],
                [0.,-1.,0.,0.,0.,0., 1., 0., 0., 0., 1., -1., 0., 0., 0., -1.],
                [0.,0.,-1.,0.,0.,0., 0., 1., 0., 1., 0., 0., -1., 0., -1., 0.],
                [0.,0.,0.,-1.,0.,0., 0., 1., 0., 0., 1., 0., -1., 0., 0., -1.],
                [0.,0.,0.,0.,-1.,0., 0., 0., 1., 1., 0., 0., 0., -1., -1., 0.],
                [0.,0.,0.,0.,0.,-1., 0., 0., 1., 0., 1., 0., 0., -1., 0., -1.]])
    h = matrix([0.,0.,0.,0.,0.,0.,7., 2., 4., 5., 8, -7., -2., -4., -5., -8])
    sol = solvers.lp(c, G, h)
    return sol['x'], sol['primal objective']
#prob2()


def prob3():
    """Find the minimizer and minimum of

    g(x,y,z) = (3/2)x^2 + 2xy + xz + 2y^2 + 2yz + (3/2)z^2 + 3x + z

    Returns (in order):
        The optimizer (sol['x'])
        The optimal value (sol['primal objective'])
    """
    Q = matrix([[3.,2.,1.],
                [2.,4.,2.],
                [1.,2.,3.]])
    p = matrix([3.,0.,1.])
    sol = solvers.qp(Q,p)
    return sol['x'], sol['primal objective']
#prob3()

def prob4():
    """Solve the allocation model problem in 'ForestData.npy'.
    Note that the first three rows of the data correspond to the first
    analysis area, the second group of three rows correspond to the second
    analysis area, and so on.

    Returns (in order):
        The optimizer (sol['x'])
        The optimal value (sol['primal objective']*-1000)
    """
    data = np.load('ForestData.npy')

    cnp = -1.*data[:,3]
    c = matrix(cnp)

    Gnp = np.array([[1.,1.,1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                    [-1.,-1.,-1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,1.,1.,1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,-1.,-1.,-1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,1.,1.,1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,-1.,-1.,-1.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,0.,0.,0.,-1.,-1.,-1.,0.,0.,0.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,-1.,-1.,-1.,0.,0.,0.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,-1.,-1.,-1.,0.,0.,0.],
                    [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,1.,1.,1.],
                    [0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,-1.,-1.,-1.]])
    Gnp = np.vstack((Gnp, -data[:,4].T, -data[:,5].T, -data[:,6].T, -1*np.eye(21)))
    G = matrix(Gnp)
    
    h = matrix([75., -75, 90., -90., 140., -140., 60., -60., 212., -212., 98., -98., 113., -113.,-40000.,-5., -70.*788. ,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.])

    sol = solvers.lp(c,G,h)
    return sol['x'][:], -1000.*sol['primal objective']


print prob4()
